package com.example.apiapplication;

import com.google.gson.annotations.SerializedName;

public class RigVeda {
    @SerializedName("mandal")
    private int mandal;
    @SerializedName("sukta")
    private int sukta;
    @SerializedName("sungby")
    private String sungby;
    @SerializedName("sungbycategory")
    private String sungbycategory;
    @SerializedName("sungfor")
    private String sungfor;
    @SerializedName("sungforcategory")
    private String sungforcategory;
    @SerializedName("meter")
    private String meter;

    public int getMandal() {
        return mandal;
    }

    public void setMandal(int mandal) {
        this.mandal = mandal;
    }

    public int getSukta() {
        return sukta;
    }

    public void setSukta(int sukta) {
        this.sukta = sukta;
    }

    public String getSungby() {
        return sungby;
    }

    public void setSungby(String sungby) {
        this.sungby = sungby;
    }

    public String getSungbycategory() {
        return sungbycategory;
    }

    public void setSungbycategory(String sungbycategory) {
        this.sungbycategory = sungbycategory;
    }

    public String getSungfor() {
        return sungfor;
    }

    public void setSungfor(String sungfor) {
        this.sungfor = sungfor;
    }

    public String getSungforcategory() {
        return sungforcategory;
    }

    public void setSungforcategory(String sungforcategory) {
        this.sungforcategory = sungforcategory;
    }

    public String getMeter() {
        return meter;
    }

    public void setMeter(String meter) {
        this.meter = meter;
    }

    public RigVeda(int mandal, int sukta, String sungby, String sungbycategory, String sungfor, String sungforcategory, String meter) {
        this.mandal = mandal;
        this.sukta = sukta;
        this.sungby = sungby;
        this.sungbycategory = sungbycategory;
        this.sungfor = sungfor;
        this.sungforcategory = sungforcategory;
        this.meter = meter;
    }

    @Override
    public String toString() {
        return "RigVeda{" +
                "mandal=" + mandal +
                ", sukta=" + sukta +
                ", sungby='" + sungby + '\'' +
                ", sungbycategory='" + sungbycategory + '\'' +
                ", sungfor='" + sungfor + '\'' +
                ", sungforcategory='" + sungforcategory + '\'' +
                ", meter='" + meter + '\'' +
                '}';
    }
}
