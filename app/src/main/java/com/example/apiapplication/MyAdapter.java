package com.example.apiapplication;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;


public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> implements Filterable {
    private ArrayList<RigVeda> list;
    private ArrayList<RigVeda> fullList;
    private Context context;
    private Filter valueFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<RigVeda> filteredList = new ArrayList<RigVeda>();
            if (constraint == null || constraint.length() == 0) {
                filteredList.addAll(fullList);
            } else {
                String filterPattern = constraint.toString().toLowerCase().trim();
                for (RigVeda item : fullList) {
                    if (item.getSungby().toLowerCase().contains(filterPattern)
                            || item.getSungbycategory().toLowerCase().contains(filterPattern)
                            || item.getSungforcategory().toLowerCase().contains(filterPattern)
                            || item.getSungfor().toLowerCase().contains(filterPattern)
                            || item.getMeter().toLowerCase().contains(filterPattern)
                    ) {
                        filteredList.add(item);
                    }
                }
            }
            FilterResults results = new FilterResults();
            results.values = filteredList;
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            list.clear();
            list.addAll((List) results.values);
            Toast.makeText(context, list.size() + " result found.", Toast.LENGTH_SHORT).show();
            notifyDataSetChanged();
        }
    };


    MyAdapter(Context context, ArrayList<RigVeda> list) {
        this.context = context;
        this.list = list;
        this.fullList = (ArrayList<RigVeda>) list.clone();
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item, parent, false));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.bind(position);
    }

    public void notifyDataSetChanged1() {
        fullList.addAll(list);
        notifyDataSetChanged();
    }

    @Override
    public Filter getFilter() {
        return valueFilter;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tvMandalId, tvSuktaId, tvSingerName, tvSingerCategory, tvRag, tvSingerGender, tvMeterName;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            tvMandalId = itemView.findViewById(R.id.tv_mandal_id);
            tvSuktaId = itemView.findViewById(R.id.tv_sukta_id);
            tvSingerName = itemView.findViewById(R.id.tv_singerName);
            tvSingerCategory = itemView.findViewById(R.id.tv_singer_category);
            tvRag = itemView.findViewById(R.id.tv_rag);
            tvSingerGender = itemView.findViewById(R.id.tv_singer_gender);
            tvMeterName = itemView.findViewById(R.id.tv_meter_name);

        }

        public void bind(int position) {
            RigVeda model = list.get(position);
            tvMandalId.setText(String.valueOf(model.getMandal()));
            tvSuktaId.setText(String.valueOf(model.getSukta()));
            tvSingerName.setText(model.getSungby());
            tvSingerCategory.setText(model.getSungbycategory());
            tvRag.setText(model.getSungfor());
            tvSingerGender.setText(model.getSungforcategory());
            tvMeterName.setText(model.getMeter());


        }
    }
}
