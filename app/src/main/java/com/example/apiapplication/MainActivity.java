package com.example.apiapplication;

import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {
    private Retrofit retrofit;
    RecyclerView rv;
    MyAdapter adapter;
    ArrayList<RigVeda> list = new ArrayList<>();
    SearchView sv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initRetrofit();
        initDesign();
        getData();
    }

    public void initDesign() {
        sv = findViewById(R.id.sv);
        rv = findViewById(R.id.recyclerview);
        rv.setHasFixedSize(true);
        adapter = new MyAdapter(this, list);
        rv.setAdapter(adapter);

        sv.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                adapter.getFilter().filter(newText);
                return false;
            }
        });

    }

    public void initRetrofit() {
        retrofit = new Retrofit.Builder()
                .baseUrl(Api.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    public void getData() {
        Api api = retrofit.create(Api.class);
        Call<List<RigVeda>> call = api.getResponses();

        call.enqueue(new Callback<List<RigVeda>>() {
            @Override
            public void onResponse(Call<List<RigVeda>> call, retrofit2.Response<List<RigVeda>> rigvedas) {
                list.clear();
                list.addAll(rigvedas.body());
                adapter.notifyDataSetChanged1();

                for (RigVeda model : list) {
                    Log.d("Log_Data", model.toString());
                }
            }

            @Override
            public void onFailure(Call<List<RigVeda>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
                Log.d("Fail", t.getMessage());
            }
        });
    }

}