package com.example.apiapplication;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface Api {

    String BASE_URL="https://sheetlabs.com/IND/";

    @GET("rv")
    Call<List<RigVeda>> getResponses();
}
